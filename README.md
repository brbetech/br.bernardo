## Hi, I'm Bernardo.

I'm [Code's](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/y2sfhdppyrxkkn5srewg.jpg)  father and [Daniela's](https://www.instagram.com/danifsantos.br/) husband.

<div>
    <p >
    <a href="https://www.linkedin.com/in/brbernardo" target="_blank">
        <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" alt="">
    </a>
    <a href="https://dev.to/bernardo">
        <img src="https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=dev.to&logoColor=white" alt="">
    </a>
    <a href="https://account.xbox.com/pt-BR/Profile?xr=BrBernardo0">
      <img src="https://img.shields.io/badge/Xbox-107C10?style=for-the-badge&logo=xbox&logoColor=white" alt="">
    </a>
    <a href="https://github.com/brbernardo/brbernardo/blob/main/How-to-Add-Friends-Nintendo-Switch.md">
      <img src="https://img.shields.io/badge/Nintendo_Switch-E60012?style=for-the-badge&logo=nintendo-switch&logoColor=white" alt="">
    </a>
    <a href="https://www.playstation.com/pt-br/support/account/add-friends-psn/">
      <img src="https://img.shields.io/badge/PlayStation-003791?style=for-the-badge&logo=playstation&logoColor=white" alt="">
    </a>
    </p>
</div>
---

![Working](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/lrbimr5bcxmevdp8h704.gif)


---

## **About Me**

### A mix of personal and trivial:

- I was born and raised in [Rio de Janeiro](https://pt.wikipedia.org/wiki/Rio_de_Janeiro) and I'm currently living in [São Paulo](https://pt.wikipedia.org/wiki/S%C3%A3o_Paulo)
- I specialize in Internet systems, centering my practices and learning around Finops and DevSecOPS engineering to help my teams deliver software with operational excellence, security, reliability, performance efficiency and cost optimization.
- I am proud of the relationships I build at work. My goal is to have an authentic, open and trusting relationship with you. I'm not going to lie to you about how I feel, or what's going on inside my head. I will work to build a sense of companionship, so please get in touch if you want to chat!
- I always assume that you are very good at your job and that you are here to add value to our company and the people around you. You are also the expert. I will work to provide the necessary context and ask questions to help you get your ideas, but I will not replace it.
- I value connecting with my team regularly. I'm a 1:1s fan and I also love to experience the almost "personal" interactions of a video call
- As a mentor/manager, I believe that these conversations are guided based on the agenda you want to set. What would you like to talk about? What's going well? What is bothering you? How can I help you?
- If there are things I want to ask you, I will do it, but this is your turn. If I have feedback, I will also provide it during that time.
- I try to write regularly in [dev.to/bernardo](https://dev.to/bernardo)
- I write some provocations on [Linkedin](https://www.linkedin.com/in/brbernardo/) and [Twitter](https://twitter.com/_BrBernardo)


---
## **Community Contributions**

![Snake animation](https://raw.githubusercontent.com/codethi/codethi/69645ac9673d2bb64039e312397effbc05d19356/github-contribution-grid-snake.svg)

**Talks**

- [DevOpsDays Vitória 2022](https://devopsdays.org/events/2022-vitoria/welcome/) - FinOps na pratica
- [HashiTalks: Brasil 2021](https://events.hashicorp.com/hashitalksbrasil2021) - [Testando infraestrutura com Terraform](https://www.youtube.com/watch?v=sQdxLv5xZf0&list=PL81sUbsFNc5agdrEMDtU6IGyxBMv6Fq8i&index=17)
- [TDC Innovation 2021](https://thedevconf.com/tdc/2021/innovation/trilha-management-e-gestao-agil?) - Desempenho individual em um mundo não tão ágil
- [Agile Trends 2021](https://agiletrendsbr.com/programacaocolisao/) - Desenvolvimento Ágil de aplicações seguras

**Awards**

- [AWS Community Builders 2021](https://aws.amazon.com/pt/developer/community/community-builders/)
- [Gitlab Hero 2022](https://about.gitlab.com/community/heroes/members/)
- [Gitkraken Ambassador 2021](https://www.gitkraken.com/ambassador)

**Publication**

- [Jornada Ágil de Liderança](https://www.amazon.com.br/Jornada-%C3%81gil-Lideran%C3%A7a-Antonio-Muniz/dp/6588431066/ref=sr_1_29?__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=luiz+felipe+bernardo&qid=1639434048&sr=8-29&ufe=app_do%3Aamzn1.fos.6121c6c4-c969-43ae-92f7-cc248fc6181d)
---

![stats github](https://github-readme-stats.vercel.app/api?username=brbernardo&theme=blue-green)
![stats github](https://github-readme-stats.vercel.app/api/top-langs/?username=brbernardo&theme=blue-green)

---
