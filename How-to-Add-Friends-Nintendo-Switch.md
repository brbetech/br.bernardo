# Como Adicionar Amigos

**Aplica-se a:  Família Nintendo Switch, Nintendo Switch, Nintendo Switch Lite, Nintendo Switch - Modelo OLED**

---

Neste artigo, você aprenderá como adicionar amigos no Nintendo Switch.

### **Importante**

- Você deve [vincular sua conta de usuário a uma conta Nintendo](https://en-americas-support.nintendo.com/app/answers/detail/a_id/22406)
    
    antes de enviar ou receber solicitações de amizade.
    
- Existem duas maneiras de se tornar amigo. Você deve aceitar uma solicitação de amizade que recebeu ou o outro usuário deve aceitar uma solicitação de amizade enviada por você.
- Se você não vir nenhuma das opções listadas abaixo, certifique-se de ter [atualizado seu sistema para a versão mais recente do menu do sistema](https://en-americas-support.nintendo.com/app/answers/detail/a_id/22418)

## Conclua essas etapas

1. Selecione seu **ícone de usuário (Minha página)Adicionar amigo**
    
    no menu HOME e selecione
    
    ![https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/nintendo-page.png](https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/nintendo-page.png)
    
    *Um ícone de usuário vermelho com o rosto de Mario é selecionado no canto superior esquerdo do menu Nintendo Switch HOME*
    
    ![https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/add-frineds-nintendo.png](https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/add-frineds-nintendo.png)
    
    *Adicionar amigo destacado no menu Minha página*
    
2. Nessa tela, você pode aceitar solicitações de amizade pendentes que foram enviadas a você, ver as solicitações de amizade pendentes que você enviou ou usar os seguintes métodos para enviar solicitações de amizade.
    - **Sugestões de amigos:** Selecione esta opção para enviar pedidos de amizade a usuários de quem você é amigo em aplicativos para dispositivos inteligentes da Nintendo, Wii U, Nintendo 3DS, Facebook ou Twitter.
        
        ### **Importante**
        
        - Você deve [vincular seu Nintendo Network ID à sua conta Nintendo](https://en-americas-support.nintendo.com/app/answers/detail/a_id/15990/)
            
            antes de ter visibilidade para seus amigos Wii U e Nintendo 3DS.
            
        - Você deve entrar em sua conta do Facebook ou Twitter antes de ter visibilidade de seus amigos nesses serviços.
    - **Pesquisar por usuários locais:** Selecione esta opção para trocar solicitações de amizade com usuários próximos.
        
        **Importante:** Se o console Nintendo Switch não estiver conectado à Internet, o pedido de amizade será salvo temporariamente no console. O pedido de amizade será enviado automaticamente na próxima vez que o console se conectar à Internet.
        
    - **Procurar por usuários com quem você jogou:** Selecione esta opção para enviar pedidos de amizade para usuários com quem você jogou em partidas da Internet ou outro jogo online.
    - **Pesquisar com Código de Amigo:** Selecione esta opção para enviar um pedido de amizade a outro usuário digitando seu código de amigo.
        
        **Meu Friend Code é esse:**
        
        ![https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/nintendo-frind-code.jpeg](https://raw.githubusercontent.com/brbernardo/brbernardo/main/assets/images/nintendo-frind-code.jpeg)
        
    - **Amigos sugeridos:** Se você vinculou outro aplicativo (Super Mario Run, etc.) à mesma conta Nintendo, os usuários de quem você é amigo naquele aplicativo são exibidos para que você possa enviar-lhes facilmente pedidos de amizade.
        
        **Importante:** se você não quiser que as sugestões de amigos sejam exibidas, [altere a configuração do recurso Sugestão de amigos](https://en-americas-support.nintendo.com/app/answers/detail/a_id/15987) .